package main;
import database.*;

public class DbApp {
    public static void main(String[] args) {
        Location location_ap = new Location(2008,"Chisinau","Gh. Asachi str 12");
        Location location_v = new Location(2200,"New York","Brooklin");
        Location location_g = new Location(2777,"Anenii Noi","s. Tintareni");

        Offer offer_ap = new Offer("USD", 50000,"50% advance payment");
        Offer offer_v = new Offer("USD", 300,"100% advance payment");
        Offer offer_g = new Offer("MDL", 12000,"Upfront payment");

        Apartment my_appartment = new Apartment(3,location_ap,offer_ap,100f);
        my_appartment.toFile();
        //System.out.println(my_appartment);

        Villa villa = new Villa(location_v,offer_v,333f,4);
        villa.toFile();

        Garage garage = new Garage(location_g,offer_g,10f,15);
        garage.toFile();

        my_appartment.fromFile();
        villa.fromFile();
        garage.fromFile();
    }
}
