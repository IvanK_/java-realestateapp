package database;

public class RealEstate {
    private Location location;
    private Offer offer;
    private Float area;

    public RealEstate(Location location, Offer offer, Float area) {
        this.setLocation(location);
        this.setOffer(offer);
        this.setArea(area);
    }

    //Getters and Setters

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Offer getOffer() {
        return offer;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public Float getArea() {
        return area;
    }
    public void setArea(Float area) {
        if (area>=10) this.area=area;
        else System.err.println("ERROR wrong value of area (>=10)");
    }

}
