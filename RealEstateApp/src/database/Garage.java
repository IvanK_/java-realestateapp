package database;

import java.io.*;

public class Garage extends RealEstate implements RealEstateInterface {
    private Integer height;

    public Garage(Location location, Offer offer, Float area, Integer height) {
        super(location, offer, area);
        this.height = height;
    }

    //GETTERS AND SETTERS
    public Integer getHeight() {
        return height;
    }
    public void setHeight(Integer height) {
        if(height<=0) System.out.println("ERROR: Height can't be 0 or negative");
        else this.height = height;
    }

    //INTERFACE METHODS IMPLEMENTATION
    @Override
    public void toFile() {
        String file_name=this.hashCode()+".txt";
        try {
            BufferedWriter bf = new BufferedWriter(new FileWriter(file_name));
            bf.write(this.toString());
            bf.write(String.format("\n"));
            bf.write(getLocation().toString());
            bf.write(getOffer().toString());

            bf.close();
        } catch (IOException e) {
            System.err.println("CANNOT SAVE FILE");
        }
    }

    @Override
    public void fromFile() {
        String file_name = this.hashCode()+".txt";
        String read = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(file_name));
            try {
                while ((read = br.readLine())!=null) {
                    System.out.println(read);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("_____________________________");
    }

    //CASTING
    public String toString() {
        return String.format("Garage info.%n%n"+"Height:%d%n"+"Area:%.1f",getHeight(),getArea());
        //+"\t%s\n"+"\t%.2f\n"+"---------------\n"+"\t%.0f"+" m2 ("+"%d"+" rooms)",
        //----?: 1 room / 8 rooms
        //getLocation(), getOffer(), getOffer(), getRoomsCount());
    }
}
