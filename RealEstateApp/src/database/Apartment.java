package database;
import java.io.*;

public class Apartment extends RealEstate implements RealEstateInterface {
    private Integer rooms_count;

    public Apartment(Integer rooms_count, Location location, Offer offer, Float area) {
        super(location,offer,area);
        this.setRoomsCount(rooms_count);
    }

    //GETTERS AND SETTERS

    public Integer getRoomsCount() {
        return rooms_count;
    }
    public void setRoomsCount(Integer rooms_count) {
        if (rooms_count>=1) this.rooms_count=rooms_count;
        else System.err.println("ERROR wrong value of rooms (>=1)");
    }

    //IMPLEMENTATION OF INTERFACE METHODS

    @Override
    public void toFile() {
        String file_name=this.hashCode()+".txt";
        try {
            BufferedWriter bf = new BufferedWriter(new FileWriter(file_name));
            bf.write(this.toString());
            bf.write(String.format("\n"));
            bf.write(getLocation().toString());
            bf.write(getOffer().toString());

            bf.close();
        } catch (IOException e) {
            System.err.println("CANNOT SAVE FILE");
        }
    }

    @Override
    public void fromFile() {
        String file_name = this.hashCode()+".txt";
        String read = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(file_name));
            try {
                while ((read = br.readLine())!=null) {
                    System.out.println(read);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("_____________________________");
    }

    //CASTING
    public String toString() {
        return String.format("Apartment info.%n%n"+"Number of rooms:%d%n"+"Area:%.1f",getRoomsCount(),getArea());
                //+"\t%s\n"+"\t%.2f\n"+"---------------\n"+"\t%.0f"+" m2 ("+"%d"+" rooms)",
                //----?: 1 room / 8 rooms
                //getLocation(), getOffer(), getOffer(), getRoomsCount());
    }
}
