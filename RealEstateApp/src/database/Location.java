package database;


public class Location {
    private Integer mail_code;
    private String city;
    private String address;


    public Location(Integer mail_code, String city, String address) {
        this.setMail_code(mail_code);
        this.setCity(city);
        this.setAddress(address);
    }

    //getters and setters
    public Integer getMail_code() {
        return mail_code;
    }

    public void setMail_code(Integer mail_code) {
        if(mail_code<=0) System.out.println("ERROR: mail code can't be <=0");
        else this.mail_code = mail_code;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    //CASTING
    public String toString() {
        return String.format("%n%nLOCATION.%n"+"Mail Code: %d%n"+"City: %s%n"+"Address: %s%n",
                //----?: 1 room / 8 rooms
                this.getMail_code(), this.getCity(), this.getAddress());
    }
}
