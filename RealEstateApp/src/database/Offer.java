package database;

public class Offer {
    private String currencty;
    private Integer amount;
    private String offer_conditions;

    public Offer(String currencty, Integer amount, String offer_conditions) {
        this.setCurrencty(currencty);
        this.setAmount(amount);
        this.setOffer_conditions(offer_conditions);
    }

    //Getters and Setters
    public String getCurrencty() {
        return currencty;
    }

    public void setCurrencty(String currencty) {
        this.currencty = currencty;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        if (amount<=0) System.out.println("ERROR: Amount can't be negative or equal to zero");
        else if (amount > 1000000000) System.out.println("ERROR: We live in Moldova. Please, place a realistic price.");
        else this.amount = amount;
    }

    public String getOffer_conditions() {
        return offer_conditions;
    }

    public void setOffer_conditions(String offer_conditions) {
        this.offer_conditions = offer_conditions;
    }

    //CASTING
    public String toString() {
        return String.format("%n%nOFFER.%n"+"Currency: %s%n"+"Amount: %d%n"+"Offer Conditions: %s%n",
                //----?: 1 room / 8 rooms
                this.getCurrencty(), this.getAmount(), this.getOffer_conditions());
    }
}
