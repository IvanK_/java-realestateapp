package database;

import java.io.*;

public class Villa extends RealEstate implements RealEstateInterface{
    private Integer floors;

    public Villa(Location location, Offer offer, Float area, Integer floors) {
        super(location, offer, area);
        this.setFloors(floors);
    }

    //GETTERS AND SETTERS
    public Integer getFloors() {
        return floors;
    }

    public void setFloors(Integer floors) {
        if (floors<=0) System.out.println("ERROR: 0 or negative number of floors");
        else this.floors = floors;
    }


    //INTERFACE METHODS IMPLEMENTATION
    @Override
    public void toFile() {
        String file_name=this.hashCode()+".txt";
        try {
            BufferedWriter bf = new BufferedWriter(new FileWriter(file_name));
            bf.write(this.toString());
            bf.write(String.format("\n"));
            bf.write(getLocation().toString());
            bf.write(getOffer().toString());

            bf.close();
        } catch (IOException e) {
            System.err.println("CANNOT SAVE FILE");
        }
    }

    @Override
    public void fromFile() {
        String file_name = this.hashCode()+".txt";
        String read = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(file_name));
            try {
                while ((read = br.readLine())!=null) {
                    System.out.println(read);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("_____________________________");
    }

    //CASTING
    public String toString() {
        return String.format("Villa info.%n%n"+"Number of floors:%d%n"+"Area:%.1f",getFloors(),getArea());
        //+"\t%s\n"+"\t%.2f\n"+"---------------\n"+"\t%.0f"+" m2 ("+"%d"+" rooms)",
        //----?: 1 room / 8 rooms
        //getLocation(), getOffer(), getOffer(), getRoomsCount());
    }
}
